# lingua
R7RS Scheme libraries for computing with simple linguistic abstractions; `lingua` will be a collection of libraries used in the development of compilers and interpreters for computer programming languages.

Some of the libraries are a work in progress and are subject to change.

---

Eduardo Acuña Yeomans - 2015
